import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { ProductComponent } from './product/product.component';
import { CategoryComponent } from './category/category.component';
import { SimpleNotificationsModule, NotificationsService } from 'angular2-notifications';
import { appRoutes } from './app.routes';
import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';
import { CartComponent } from './cart/cart.component';
import {CartService} from './cart/cart.service';
import { CartSummaryComponent } from './cart/cart-summary/cart-summary.component';
import { VatPipe } from './product/vat.pipe';


@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    CategoryComponent,
    CartComponent,
    CartSummaryComponent,
    VatPipe
  ],
  imports: [
    BrowserModule,
    HttpModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    RouterModule.forRoot(appRoutes),
    SimpleNotificationsModule.forRoot()
  ],
  providers: [NotificationsService, CartService],
  bootstrap: [AppComponent]
})
export class AppModule { }
