import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'vat'
})
export class VatPipe implements PipeTransform {

  transform(value: number, args?: number): any {
    if (!args) {
      args = 18;
    }
    return (value * args / 100) + value;
  }

}
