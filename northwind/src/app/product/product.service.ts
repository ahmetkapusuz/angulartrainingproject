import { Injectable } from '@angular/core';
import { Http, Response, Request } from '@angular/http';
import { httpFactory } from '@angular/http/src/http_module';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Product } from './product';

@Injectable()
export class ProductService {

constructor(private http: Http) { }

getProducts(seoUrl): Observable<Product[]> {
  if (seoUrl) {
    return this.http.get('http://northwindapi.azurewebsites.net/api/products/' + seoUrl)
      .map(response => response.json());
  }

  return this.http.get('http://northwindapi.azurewebsites.net/api/products')
      .map(response => response.json());
}

}

