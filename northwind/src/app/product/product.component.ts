import { Component, OnInit } from '@angular/core';
import { Product } from './product';
import { ProductService } from './product.service';
import { ActivatedRoute } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import {CartService} from '../cart/cart.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  providers: [ProductService]
})
export class ProductComponent implements OnInit {
  products: Product[];
  selectedCategory: string;
  selectedProduct: string;
  title: 'Hello';

  constructor(private productService: ProductService,
              private activatedRoute: ActivatedRoute,
              private notificationsService: NotificationsService,
              private cartService: CartService) {

  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.getProducts(params['seoUrl']);
      this.selectedCategory = params['seoUrl'];
    });
  }

  getProducts(seoUrl: String) {
    this.productService.getProducts(seoUrl).subscribe(
      p => {
        this.products = p;
      }
    );
  }

  addToCart(product: Product) {
    this.selectedProduct = product.productName;
    this.cartService.addToCart(product);
    this.notificationsService.success('Successful', product.productName + ' added to Cart');
  }



}
